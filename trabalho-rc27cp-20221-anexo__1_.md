**Trabalho Prática: Integração de habilidades – 2022/1**

**Documentação**

**Aluno:Rodrigo Garcia Almeida Martinez RA: 2155443**

**Link Vídeo 1:**

**Link Vídeo 2:**




|**Sub-Rede**|**IPv6**|**IPv4**|||
| :-: | :-: | :-: | :- | :- |
||**Endereço da sub-rede**|**Endereço da sub-rede**|**Máscara de sub-rede**|**Broadcast**|
|Matriz|2001:db8:acad:2b00::/64|200.200.43.0|255.255.255.192|200.200.43.63|
|Filial 1|2001:db8:acad:2b01::/64|200.200.43.64|255.255.255.224|200.200.43.95|
|Filial 2|2001:db8:acad:2b02::/64|200.200.43.96|255.255.255.224|200.200.43.127|
|Filial 3|2001:db8:acad:2b03::/64|200.200.43.128|255.255.255.224|200.200.43.159|
|Filial 4|2001:db8:acad:2b04::/64|200.200.43.160|255.255.255.224|200.200.43.191|
|Filial 5|2001:db8:acad:2b05::/64|200.200.43.192|255.255.255.224|200.200.43.223|
|pb-vit|2001:db8:acad:2bff::/112|200.200.43.224|255.255.255.252|200.200.43.227|
|vit-fb|2001:db8:acad:2bff::1:0/112|200.200.43.228|255.255.255.252|200.200.43.231|
|fb-ita|2001:db8:acad:2bff::2:0/112|200.200.43.232|255.255.255.252|200.200.43.235|
|ita-pb|2001:db8:acad:2bff::3:0/112|200.200.43.236|255.255.255.252|200.200.43.239|
|cv-ita|2001:db8:acad:2bff::4:0/112|200.200.43.240|255.255.255.252|200.200.43.243|
**Quadro 1 – Sub-Redes**


|**Tabela de Endereçamento**||||||
| :-: | :- | :- | :- | :- | :-: |
|**Dispositivo**|**Interface**|**IPv4**|**Máscara de subrede**|**IPv4 Gateway**||
|||**IPv6 / Prefixo**||**IPv6 Gateway**||
|PC1|NIC|200.200.43.3|255.255.255.192|200.200.43.1||
|||2001:db8:acad:2b00::3/64||2001.db8.acad.2b00::1||
|PC2|NIC|200.200.43.4|255.255.255.192|200.200.43.1||
|||2001:db8:acad:2b00::4/64||2001.db8.acad.2b00::1||
|PC3|NIC|200.200.43.67|255.255.255.224|200.200.43.65||
|||2001:db8:acad:2b01::3/64||2001.db8.acad.2b01::1||
|PC4|NIC|200.200.43.68|255.255.255.224|200.200.43.65||
|||2001:db8:acad:2b01::4/64||2001.db8.acad.2b01::1||
|PC5|NIC|200.200.43.99|255.255.255.224|200.200.43.97||
|||2001:db8:acad:2b02::3/64||2001.db8.acad.2b02::1||
|PC6|NIC|200.200.43.100|255.255.255.224|200.200.43.97||
|||2001:db8:acad:2b02::4/64||2001.db8.acad.2b02::1||
|Switch-Matriz|SVI|200.200.43.2|255.255.255.192|200.200.43.1||
|Switch-Filial1|SVI|200.200.43.66|255.255.255.224|200.200.43.65||
|Switch-Filial2|SVI|200.200.43.98|255.255.255.224|200.200.43.97||
|Roteador Pato Branco |Fa0/0|200.200.43.1|255.255.255.192|||
|||2001:db8:acad:2b00::1/64||||
|||fe80::1||||
|Roteador Pato Branco |Se0/0/0|200.200.43.225|255.255.255.252|||
|||2001:db8:acad:2bff::1/112||||
|||EUI-64||||
|Roteador Pato Branco |Se0/0/1|200.200.43.238|255.255.255.252|||
|||2001:db8:acad:2bff::3:1/112||||
|||EUI-64||||
| Roteador  Fco. Beltrão |Fa0/0|200.200.43.65|255.255.255.224|||
|||2001:db8:acad:2b01::1/64||||
|||fe80::1||||
| Roteador  Fco. Beltrão|Se0/0/0|200.200.43.233|255.255.255.252|||
|||2001:db8:acad:2bff::2:1/112||||
|||EUI-64||||
| Roteador  Fco. Beltrão|Se0/0/1|200.200.43.230|255.255.255.252|||
|||2001:db8:acad:2bff::1:2/112||||
|||EUI-64||||
|Roteador Vitorino |Se0/0/0|200.200.43.229|255.255.255.252|||
|||2001:db8:acad:2bff::1:1/112||||
|||EUI-64||||
|Roteador Vitorino |Se0/0/1|200.200.43.226|255.255.255.252|||
|||2001:db8:acad:2bff::2/112||||
|||EUI-64||||
| Roteador Itapejara |Se0/0/0|200.200.43.237|255.255.255.252|||
|||2001:db8:acad:2bff::3:1/112||||
|||EUI-64||||
| Roteador Itapejara |Se0/0/1|200.200.43.234|255.255.255.252|||
|||2001:db8:acad:2bff::2:2/112||||
|||EUI-64||||
| Roteador Itapejara |Fa0/1|200.200.43.241|255.255.255.252|||
|||2001:db8:acad:2bff::4:1/112||||
|||EUI-64||||
|Roteador Coronel Vivida |Fa0/0|200.200.43.97|255.255.255.224|||
|||2001:db8:acad:2b02::1/64||||
|||fe80::1||||
|Roteador Coronel Vivida |Fa0/1|200.200.43.242|255.255.255.252|||
|||2001:db8:acad:2bff::4:2/112||||
|||EUI-64||||
**Quadro 2 – Endereçamento dos dispositivos**


|**Roteador Pato Branco**||||
| :-: | :- | :- | :- |
|**IPv4**||||
|Rede de Destino|Máscara|Next Hop|Interface de Saída|
|200.200.43.0|255.255.255.192|-|Fa0/0|
|200.200.43.64|255.255.255.224|-|Se0/0/0|
|200.200.43.96|255.255.255.224|200.200.43.226|Se0/0/0|
|200.200.43.224|255.255.255.252|200.200.43.226|Se0/0/0|
|200.200.43.228|255.255.255.252|200.200.43.226|Se0/0/0|
|200.200.43.232|255.255.255.252|200.200.43.226|Se0/0/0|
|200.200.43.236|255.255.255.252|200.200.43.226|Se0/0/0|
|200.200.43.240|255.255.255.252|200.200.43.226|Se0/0/0|
|**IPv6**||||
|Rede de Destino/Prefixo||Next Hop|Interface de Saída|
|2001:db8:acad:2b00::/64||-|Fa0/0|
|2001:db8:acad:2b01::/64||-|Se0/0/0|
|2001:db8:acad:2b02::/64||2001:db8:acad:2bff::2/112|Se0/0/0|
|2001:db8:acad:2bff::/112||2001:db8:acad:2bff::2/112|Se0/0/0|
|2001:db8:acad:2bff::1:0/112||2001:db8:acad:2bff::2/112|Se0/0/0|
|2001:db8:acad:2bff::2:0/112||2001:db8:acad:2bff::2/112|Se0/0/0|
|2001:db8:acad:2bff::3:0/112||2001:db8:acad:2bff::2/112|Se0/0/0|
|2001:db8:acad:2bff::4:0/112||2001:db8:acad:2bff::2/112|Se0/0/0|
**Quadro 3 – Tabela de Roteamento do Roteador Pato Branco**

|**Roteador Francisco Beltrão**||||
| :-: | :- | :- | :- |
|**IPv4**||||
|Rede de Destino|Máscara|Next Hop|Interface de Saída|
|200.200.43.0|255.255.255.192|200.200.43.234|Se0/0/0|
|200.200.43.64|255.255.255.224|-|Fa0/0|
|200.200.43.96|255.255.255.224|200.200.43.234|Se0/0/0|
|200.200.43.224|255.255.255.252|-|Se0/0/0|
|200.200.43.228|255.255.255.252|200.200.43.234|Se0/0/0|
|200.200.43.232|255.255.255.252|-|Se0/0/0|
|200.200.43.236|255.255.255.252|200.200.43.234|Se0/0/0|
|200.200.43.240|255.255.255.252|200.200.43.234|Se0/0/0|
|**ipv6**||||
|Rede de Destino/Prefixo||Next Hop|Interface de Saída|
|2001:db8:acad:2b00::/64||2001:db8:acad:2bff::2:2/112|Se0/0/0|
|2001:db8:acad:2b01::/64||-|Fa0/0|
|2001:db8:acad:2b02::/64||2001:db8:acad:2bff::2:2/112|Se0/0/0|
|2001:db8:acad:2bff::/112||2001:db8:acad:2bff::2:2/112|Se0/0/0|
|2001:db8:acad:2bff::1:0/112||2001:db8:acad:2bff::2:2/112|Se0/0/0|
|2001:db8:acad:2bff::2:0/112||-|Se0/0/0|
|2001:db8:acad:2bff::3:0/112||2001:db8:acad:2bff::2:2/112|Se0/0/0|
|2001:db8:acad:2bff::4:0/112||2001:db8:acad:2bff::2:2/112|Se0/0/0|
**Quadro 4 – Tabela de Roteamento do Roteador Francisco Beltrão**

|**Roteador Vitorino**||||
| :-: | :- | :- | :- |
|**IPv4**||||
|Rede de Destino|Máscara|Next Hop|Interface de Saída|
|200.200.43.0|255.255.255.192|200.200.43.230|Se0/0/0|
|200.200.43.64|255.255.255.224|200.200.43.230|Se0/0/0|
|200.200.43.96|255.255.255.224|200.200.43.230|Se0/0/0|
|200.200.43.224|255.255.255.252|200.200.43.230|Se0/0/0|
|200.200.43.228|255.255.255.252|-|Se0/0/0|
|200.200.43.232|255.255.255.252|200.200.43.230|Se0/0/0|
|200.200.43.236|255.255.255.252|200.200.43.230|Se0/0/0|
|200.200.43.240|255.255.255.252|200.200.43.230|Se0/0/0|
|**IPv6**||||
|Rede de Destino/Prefixo||Next Hop|Interface de Saída|
|2001:db8:acad:2b00::/64||2001:db8:acad:2bff::1:2/112|Se0/0/0|
|2001:db8:acad:2b01::/64||2001:db8:acad:2bff::1:2/112|Se0/0/0|
|2001:db8:acad:2b02::/64||2001:db8:acad:2bff::1:2/112|Se0/0/0|
|2001:db8:acad:2bff::/112||2001:db8:acad:2bff::1:2/112|Se0/0/0|
|2001:db8:acad:2bff::1:0/112||-|Se0/0/0|
|2001:db8:acad:2bff::2:0/112||2001:db8:acad:2bff::1:2/112|Se0/0/0|
|2001:db8:acad:2bff::3:0/112||2001:db8:acad:2bff::1:2/112|Se0/0/0|
|2001:db8:acad:2bff::4:0/112||2001:db8:acad:2bff::1:2/112|Se0/0/0|
**Quadro 5 – Tabela de Roteamento do Roteador Vitorino**

|**Roteador Itapejara D’Oeste**||||
| :-: | :- | :- | :- |
|**IPv4**||||
|Rede de Destino|Máscara|Next Hop|Interface de Saída|
|200.200.43.0|255.255.255.192|200.200.43.238|Se0/0/0|
|200.200.43.64|255.255.255.224|200.200.43.238|Se0/0/0|
|200.200.43.96|255.255.255.224|200.200.43.242|Fa0/1|
|200.200.43.224|255.255.255.252|200.200.43.238|Se0/0/0|
|200.200.43.228|255.255.255.252|200.200.43.238|Se0/0/0|
|200.200.43.232|255.255.255.252|200.200.43.238|Se0/0/0|
|200.200.43.236|255.255.255.252|-|Se0/0/0|
|200.200.43.240|255.255.255.252|-|Fa0/1|
|**IPv6**||||
|Rede de Destino/Prefixo||Next Hop|Interface de Saída|
|2001:db8:acad:2b00::/64||2001:db8:acad:2bff::3:2/112|Se0/0/0|
|2001:db8:acad:2b01::/64||2001:db8:acad:2bff::3:2/112|Se0/0/0|
|2001:db8:acad:2b02::/64||2001:db8:acad:2bff::4:2/112|Fa0/1|
|2001:db8:acad:2bff::/112||2001:db8:acad:2bff::3:2/112|Se0/0/0|
|2001:db8:acad:2bff::1:0/112||2001:db8:acad:2bff::3:2/112|Se0/0/0|
|2001:db8:acad:2bff::2:0/112||2001:db8:acad:2bff::3:2/112|Se0/0/0|
|2001:db8:acad:2bff::3:0/112||-|Se0/0/0|
|2001:db8:acad:2bff::4:0/112||-|Fa0/1|
**Quadro 6 – Tabela de Roteamento do Roteador Itapejara D’Oeste**


|**Roteador Coronel Vivida**||||
| :-: | :- | :- | :- |
|**IPv4**||||
|Rede de Destino|Máscara|Next Hop|Interface de Saída|
|200.200.43.0|255.255.255.192|200.200.43.241|Fa0/1|
|200.200.43.64|255.255.255.224|200.200.43.241|Fa0/1|
|200.200.43.96|255.255.255.224|-|Fa0/0|
|200.200.43.224|255.255.255.252|200.200.43.241|Fa0/1|
|200.200.43.228|255.255.255.252|200.200.43.241|Fa0/1|
|200.200.43.232|255.255.255.252|200.200.43.241|Fa0/1|
|200.200.43.236|255.255.255.252|200.200.43.241|Fa0/1|
|200.200.43.240|255.255.255.252|-|Fa0/1|
|**IPv6**||||
|Rede de Destino/Prefixo||Next Hop|Interface de Saída|
|2001:db8:acad:2b00::/64||2001:db8:acad:2bff::4:1/112|Fa0/1|
|2001:db8:acad:2b01::/64||2001:db8:acad:2bff::4:1/112|Fa0/1|
|2001:db8:acad:2b02::/64||-|Fa0/0|
|2001:db8:acad:2bff::/112||2001:db8:acad:2bff::4:1/112|Fa0/1|
|2001:db8:acad:2bff::1:0/112||2001:db8:acad:2bff::4:1/112|Fa0/1|
|2001:db8:acad:2bff::2:0/112||2001:db8:acad:2bff::4:1/112|Fa0/1|
|2001:db8:acad:2bff::3:0/112||2001:db8:acad:2bff::4:1/112|Fa0/1|
|2001:db8:acad:2bff::4:0/112||-|Fa0/1|
**Quadro 7 – Tabela de Roteamento do Roteador Coronel Vivida**
## Topologia - Packet Tracer
- [ ] ![Trabalho2-RODRIGO GARCIA ALMEIDA MARTINEZ](trabalho2-topologia-Rodrigo_Martinez.pkt)


## Arquivos de Configuração dos Dispositivos Intermediários (roteadores e switches)
- [ ] ![Roteador Pato Branco](roteador_pato_branco.txt)
- [ ] ![Roteador Francisco Beltrão](roteador_francisco_beltrao.txt)
- [ ] ![Roteador Vitorino](roteador_vitorino.txt)
- [ ] ![Roteador Itapejara D'Oeste](roteador_itapejara.txt)
- [ ] ![Roteador Coronel Vivida]( roteador_coronel.txt)
- [ ] ![Switch Pato Branco]( switch_matriz.txt)
- [ ] ![Switch Francisco Beltrão]( switch_filial1.txt)
- [ ] ![Switch Coronel Vivida]( switch_filial2.txt)
